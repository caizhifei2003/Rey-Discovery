﻿using Consul;
using Microsoft.AspNetCore.Http.Features;
using Rey.Discovery.Configuration;

namespace Rey.Discovery
{
    public class ConsulServer : IConsulServer
    {
        private readonly IConsulClient _client;
        private readonly IConsulLocal _local;

        public ConsulServer(DiscoveryOptions options, IConsulLocal local)
        {
            this._local = local;
            this._client = new ConsulClient(cfg =>
            {
                cfg.Address = options.Consul.GetUri();
                cfg.Datacenter = options.Consul.Datacenter;
                cfg.Token = options.Consul.Token;
            });
        }

        public void Register(IFeatureCollection features)
        {
            this._local.Register(features, this._client);
        }

        public void Deregister(IFeatureCollection features)
        {
            this._local.Deregister(features, this._client);
        }
    }
}
