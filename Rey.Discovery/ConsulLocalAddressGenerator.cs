﻿using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http.Features;
using Rey.Discovery.Configuration;
using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace Rey.Discovery
{
    public class ConsulLocalAddressGenerator : IConsulLocalAddressGenerator
    {
        private readonly ConsulLocalOptions _options;

        public ConsulLocalAddressGenerator(DiscoveryOptions options)
        {
            this._options = options.Local;
        }

        private string GetFeatureHost(IFeatureCollection features)
        {
            return features.Get<IServerAddressesFeature>()
                .Addresses
                .Select(x => new Uri(x).Host)
                .First();
        }

        private string GetDnsHost()
        {
            return Dns.GetHostEntry(Dns.GetHostName())
                .AddressList
                .Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                .Select(x => x.ToString())
                .First();
        }

        public Uri Generate(IFeatureCollection features)
        {
            var host = Regex.Replace(this._options.Host, "\\{(?<name>.*?)\\}", m =>
            {
                var name = m.Groups["name"].Value;
                if (name.Equals("feature")) return this.GetFeatureHost(features);
                if (name.Equals("dns")) return this.GetDnsHost();
                throw new InvalidOperationException("invalid host name");
            });

            var scheme = this._options.Scheme ?? "http";
            var port = this._options.Port == 80 ? "" : $":{this._options.Port}";
            var url = $"{scheme}://{host}{port}";
            return new Uri(url);
        }
    }
}
