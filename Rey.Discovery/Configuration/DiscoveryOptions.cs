﻿using System;

namespace Rey.Discovery.Configuration
{
    public class DiscoveryOptions
    {
        public ConsulServerOptions Consul { get; set; }
        public ConsulLocalOptions Local { get; set; }
        public DnsOptions Dns { get; set; }
    }

    public class DiscoveryAddressOptions
    {
        public string Scheme { get; set; } = "http";
        public string Host { get; set; }
        public int Port { get; set; } = 80;
    }

    public class ConsulServerOptions : DiscoveryAddressOptions
    {
        public string Datacenter { get; set; }
        public string Token { get; set; }

        public Uri GetUri()
        {
            if (this.Scheme == null || this.Host == null)
                return null;

            var port = this.Port == 80 ? "" : $":{this.Port}";
            var url = $"{this.Scheme}://{this.Host}{port}";
            return new Uri(url);
        }
    }

    public class ConsulLocalOptions : DiscoveryAddressOptions
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string[] Tags { get; set; }
    }

    public class DnsOptions
    {
        public string Ip { get; set; }
        public int Port { get; set; }
        public string Scheme { get; set; } = "http";
    }
}
