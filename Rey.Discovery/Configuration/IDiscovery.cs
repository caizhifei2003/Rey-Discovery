﻿using System;

namespace Rey.Discovery
{
    public interface IDiscovery
    {
        Uri Discover(string name);
    }
}
