﻿using Rey.Discovery;
using Rey.Discovery.Configuration;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class DiscoveryServiceCollectionExtensions
    {
        public static IServiceCollection AddDiscovery(this IServiceCollection services, Action<DiscoveryOptions> configure = null)
        {
            var options = new DiscoveryOptions();
            configure?.Invoke(options);

            services.AddSingleton(options);
            services.AddSingleton<IConsulServer, ConsulServer>();
            services.AddSingleton<IConsulLocal, ConsulLocal>();
            services.AddSingleton<IConsulLocalAddressGenerator, ConsulLocalAddressGenerator>();
            services.AddSingleton<IDiscovery, Discovery>();

            return services;
        }
    }
}
