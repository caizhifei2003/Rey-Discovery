﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Rey.Discovery;

namespace Microsoft.AspNetCore.Builder
{
    public static class DiscoveryApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseDiscovery(this IApplicationBuilder app)
        {
            var provider = app.ApplicationServices;
            var server = provider.GetService<IConsulServer>();
            var life = app.ApplicationServices.GetService<IApplicationLifetime>();

            life.ApplicationStarted.Register(() => server.Register(app.ServerFeatures));
            life.ApplicationStopping.Register(() => server.Deregister(app.ServerFeatures));
            return app;
        }

        //private static void AppStart(IServiceProvider provider)
        //{
        //    var options = provider.GetService<DiscoveryOptions>();
        //    var client = provider.GetService<IConsulClient>();

        //    var local = options.Local;
        //    var localUri = local.GetUri();

        //    var check = new AgentServiceCheck()
        //    {
        //        DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(30),
        //        Interval = TimeSpan.FromSeconds(30),
        //        HTTP = $"{localUri}ping",
        //    };

        //    var reg = new AgentServiceRegistration()
        //    {
        //        ID = local.GetId(),
        //        Name = local.Name,
        //        Address = local.Host,
        //        Port = local.Port,
        //        Tags = local.Tags,
        //        Checks = new[] { check }
        //    };

        //    var ret = client.Agent.ServiceRegister(reg).GetAwaiter().GetResult();

        //    var hostName = Dns.GetHostName();
        //    var addrs = Dns.GetHostEntry("caizhifei.com").AddressList;

        //}

        //private static void AppStop(IServiceProvider provider)
        //{
        //    var client = provider.GetService<IConsulClient>();
        //    var local = provider.GetService<DiscoveryOptions>().Local;
        //    var ret = client.Agent.ServiceDeregister(local.GetId()).GetAwaiter().GetResult();
        //}
    }
}
