﻿using Consul;
using Microsoft.AspNetCore.Http.Features;

namespace Rey.Discovery
{
    public interface IConsulLocal
    {
        void Register(IFeatureCollection features, IConsulClient client);
        void Deregister(IFeatureCollection features, IConsulClient client);
    }
}
