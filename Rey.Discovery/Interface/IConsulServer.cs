﻿using Microsoft.AspNetCore.Http.Features;

namespace Rey.Discovery
{
    public interface IConsulServer
    {
        void Register(IFeatureCollection features);
        void Deregister(IFeatureCollection features);
    }
}
