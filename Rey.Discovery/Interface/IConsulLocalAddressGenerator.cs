﻿using Microsoft.AspNetCore.Http.Features;
using System;

namespace Rey.Discovery
{
    public interface IConsulLocalAddressGenerator
    {
        Uri Generate(IFeatureCollection features);
    }
}
