﻿using DnsClient;
using Rey.Discovery.Configuration;
using System;
using System.Linq;
using System.Net;

namespace Rey.Discovery
{
    public class Discovery : IDiscovery
    {
        private readonly DnsOptions _options;

        public Discovery(DiscoveryOptions options)
        {
            this._options = options.Dns;
        }

        public Uri Discover(string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            var nameEndPoint = new IPEndPoint(IPAddress.Parse(this._options.Ip), this._options.Port);
            var lookup = new LookupClient(nameEndPoint);
            var uri = lookup.ResolveService("service.consul", name)
                .Select(x =>
                {
                    var host = x.HostName.TrimEnd('.');
                    var port = x.Port == 80 ? "" : $":{x.Port}";
                    return new Uri($"{this._options.Scheme}://{host}{ port }");
                })
                .FirstOrDefault()
                ;
            return uri;
        }
    }
}
