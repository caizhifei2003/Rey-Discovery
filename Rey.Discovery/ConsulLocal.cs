﻿using Consul;
using Microsoft.AspNetCore.Http.Features;
using Rey.Discovery.Configuration;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Rey.Discovery
{
    public class ConsulLocal : IConsulLocal
    {
        private string _id;
        private readonly ConsulLocalOptions _options;
        private readonly IConsulLocalAddressGenerator _addressGenerator;

        public ConsulLocal(
            DiscoveryOptions options,
            IConsulLocalAddressGenerator addressGenerator
            )
        {
            this._options = options.Local;
            this._addressGenerator = addressGenerator;
        }

        public void Register(IFeatureCollection features, IConsulClient client)
        {
            //! 生成客户端地址
            var address = this._addressGenerator.Generate(features);

            //! 格式化ID
            var dic = new Dictionary<string, object>();
            dic.Add("scheme", address.Scheme);
            dic.Add("host", address.Host);
            dic.Add("port", address.Port);
            dic.Add("uri", address);
            dic.Add("name", this._options.Name);

            var id = Regex.Replace(this._options.Id, "\\{(?<name>.*?)\\}", m =>
            {
                var name = m.Groups["name"].Value;
                return $"{dic[name]}";
            });

            //! ping检查
            var check = new AgentServiceCheck()
            {
                DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(30),
                Interval = TimeSpan.FromSeconds(30),
                HTTP = $"{address}ping",
            };

            var registration = new AgentServiceRegistration()
            {
                Address = address.Host,
                Port = address.Port,
                ID = id,
                Name = this._options.Name,
                Tags = this._options.Tags,
                Checks = new[] { check }
            };

            client.Agent.ServiceRegister(registration).Wait();
            this._id = id;
        }

        public void Deregister(IFeatureCollection features, IConsulClient client)
        {
            if (this._id == null)
                return;

            client.Agent.ServiceDeregister(this._id);
            this._id = null;
        }
    }
}
